import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import { environment } from '../environments/environment';
@Injectable()
export class ProductsService {
http:Http;

getProducts(){
 return this.http.get(environment.url +'products');
}
getProductsFire(){
      return this.db.list('/products').valueChanges();
}

//get one product
getProduct(id){
  return this.http.get(environment.url +'products/'+ id);
}


//update


putProduct(data,key){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url +'products/'+key, params.toString(), options);      
  }

  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }

}
