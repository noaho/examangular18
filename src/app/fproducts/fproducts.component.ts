import { Component, OnInit } from '@angular/core';
import { ProductsService } from "../products.service";

@Component({
  selector: 'app-fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {
products;
  constructor(private service:ProductsService) { }

  ngOnInit() {
    this.service.getProductsFire().subscribe(respones=>{
      console.log(respones);
      this.products = respones;
    })
  }

}
