import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductsService } from '../products.service';

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Output() updateProduct:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateProductPs:EventEmitter<any> = new EventEmitter<any>();
  

  service:ProductsService;
  proform =new FormGroup({
    name:new FormControl(),
    price:new FormControl(),
  });
  constructor(private route: ActivatedRoute ,service:ProductsService) { 
    this.service = service;
  }
  
  
  sendData() {
    this.updateProduct.emit(this.proform.value.name);
    console.log(this.proform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putProduct(this.proform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateProductPs.emit();
        }
      );
    })
  }
  
  product;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getProduct(id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product);
      })
    })
  }

}
