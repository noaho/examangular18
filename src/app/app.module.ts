import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {environment} from './../environments/environment';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import {RouterModule} from '@angular/router';
import { ProductsService } from "./products.service";
import { HttpModule } from '@angular/http';
import { EditProductComponent } from './edit-product/edit-product.component';
import { FproductsComponent } from './fproducts/fproducts.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    EditProductComponent,
    FproductsComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpModule,
     RouterModule.forRoot([
    {path: '', component:ProductsComponent},
    {path: 'products', component:ProductsComponent},
    {path: 'login', component:LoginComponent},
    {path:'edit-product/:id', component:EditProductComponent},
    {path: 'fproducts', component:FproductsComponent},
    {path: '**', component:NotFoundComponent}
  ])
  ],
  providers: [ProductsService, HttpModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
