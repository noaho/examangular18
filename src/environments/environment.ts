// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
   url: 'http://localhost/angular/slim/',
  firebase:{
     apiKey: "AIzaSyCCttBpAnVUaMNWYfPG0d-mhREq6j1OXJY",
    authDomain: "angular-fc2f9.firebaseapp.com",
    databaseURL: "https://angular-fc2f9.firebaseio.com",
    projectId: "angular-fc2f9",
    storageBucket: "angular-fc2f9.appspot.com",
    messagingSenderId: "892546487093"
  }
};

